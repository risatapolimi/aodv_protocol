#ifndef __AODVPROTOCOL_H
#define __AODVPROTOCOL_H

enum{
	MOTES_NUMBER = 8,
	MESSAGE_ID_BASE = 1000,

	AM_CHANNEL = 94,

	DROP_REPLY_MS = 1000,
	EXPIRE_ROUTING_MS = 90000,
	DATA_TIMER_MS = 30000,

	TABLE_ROUTING_DIM = 2*MOTES_NUMBER,
	TABLE_DISCOVERY_DIM = 32*MOTES_NUMBER,
	TABLE_DATA_DIM = 16*MOTES_NUMBER
};

typedef nx_struct {
	nx_uint16_t final_destination_id; 
	nx_uint16_t initial_source_id;
	nx_uint32_t request_id;
	nx_uint16_t forward_hops;
} msg_request_t;

typedef nx_struct {
	nx_uint16_t backward_hops;  
	nx_uint16_t requester_node;
	nx_uint16_t replyer_node;
	nx_uint32_t request_id;
	nx_uint8_t reply_flag;	
} msg_reply_t;

typedef nx_struct {   
	nx_uint16_t final_destination_id;
	nx_uint16_t source_id;
	nx_uint32_t data;	
} msg_data_t;

typedef struct{
	uint32_t request_id;	
	msg_data_t message;
} entry_data_t;

typedef struct {   
	uint16_t destination;	
	uint16_t next_hop;
	bool status;	
} entry_route_t;

typedef struct {   
	uint32_t request_id;	
	uint16_t initial_source_id;
	uint16_t sender_id;
	uint8_t forward_cost;
	uint8_t residual_cost;	
	uint32_t timestamp;
} entry_discovery_t;

typedef struct {
	entry_route_t entries[TABLE_ROUTING_DIM];
	uint16_t first_free;
} table_route_t;

typedef struct {
	entry_discovery_t entries[TABLE_DISCOVERY_DIM];
	uint16_t first_free;
} table_discovery_t;

typedef struct{
	entry_data_t entries[TABLE_DATA_DIM];
	uint16_t first_free;
} table_data_t;

enum{
	MSG_REQ_LEN = sizeof(msg_request_t),
	MSG_REPLY_LEN = sizeof(msg_reply_t),
	MSG_DATA_LEN = sizeof(msg_data_t)
};


#endif