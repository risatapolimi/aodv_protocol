#include "Timer.h"
#include "aodv_protocol.h"
#include <stdio.h>

module AodvProtocolC{
	
	uses {

		interface Boot;

		interface AMSend;
		interface Receive;
		interface AMPacket;
		interface Packet;

		interface Timer<TMilli> as DataTimer;
		interface Timer<TMilli> as Destination1;
		interface Timer<TMilli> as Destination2;
		interface Timer<TMilli> as Destination3;
		interface Timer<TMilli> as Destination4;
		interface Timer<TMilli> as Destination5;
		interface Timer<TMilli> as Destination6;
		interface Timer<TMilli> as Destination7;
		interface Timer<TMilli> as Destination8;

		interface SplitControl as AMControl;

		interface Random;

	}
	
}

implementation{

	table_route_t routing_table;
	table_discovery_t discovery_table;
	table_data_t data_table;
	message_t packet;
	nx_uint16_t destination;
	uint32_t local_msg_id;
	bool busy = FALSE;

	/*
	* Function declaration
	*/
	
	void print_routing_table();
	void print_routing_discovery_table();
	void manage_request(am_addr_t source_address, void* msg_body);
	void manage_reply(am_addr_t source_address, void* msg_body);
	void manage_data(am_addr_t source_address, void* msg_body);
	void send_rreq(nx_uint16_t dest);
	void send_rreply(msg_request_t* req_msg,am_addr_t source_address);
	void send_data(nx_uint16_t next_hop,nx_uint32_t random_data);
	void fwd_request(msg_request_t* request_msg);
	void fwd_reply(msg_reply_t* route_reply, nx_uint16_t sender_id);
	void fwd_data(nx_uint16_t next_hop,msg_data_t* msg);
	void expire_destination (uint16_t routing_destination);
	void start_destination_timer(uint16_t routing_destination);

	/*
	* Events actions
	*/

	event void Boot.booted() {
		routing_table.first_free = 0;
		discovery_table.first_free = 0;
		data_table.first_free = 0;
		local_msg_id = TOS_NODE_ID*MESSAGE_ID_BASE;

    	call AMControl.start();

    	printf("Mote booted successfully\n");
  	}

  	event void AMControl.startDone(error_t err) {
    	if (err == SUCCESS) {
      		call DataTimer.startPeriodic(DATA_TIMER_MS + (TOS_NODE_ID*110));
    	}
    	else {
      		call AMControl.start();
    	}
  	}

  	event void AMControl.stopDone(error_t err) {
  		printf("Radio turned off\n");
  	}

  	event void DataTimer.fired(){
  		bool found=FALSE;
  		uint8_t k=0;
  		uint32_t current_time;
  		uint8_t i;
  		uint8_t roll = 0;
  		nx_uint32_t random_data;

  		print_routing_table();

  		do{
  			destination=(call Random.rand16() % MOTES_NUMBER)+1;
  			roll++;
  		}while (destination == TOS_NODE_ID && roll<10);

  		random_data=call Random.rand16();

  		i = 0;
  		while(i<routing_table.first_free){
  			if(routing_table.entries[i].destination == destination && routing_table.entries[i].status){
  				k = i;
  				i = routing_table.first_free;
  				found=TRUE;
  			}
  			i++;
  		}
  	
  		if(found){
  			send_data(routing_table.entries[k].next_hop,random_data);
  		}
  		else{
  			current_time = call DataTimer.getNow();

  			data_table.entries[data_table.first_free].request_id=local_msg_id;
  			data_table.entries[data_table.first_free].message.data=random_data;
  			data_table.entries[data_table.first_free].message.final_destination_id=destination;
  			data_table.entries[data_table.first_free].message.source_id=TOS_NODE_ID;
  			data_table.first_free++;
  
  			discovery_table.entries[discovery_table.first_free].request_id = local_msg_id;
  			discovery_table.entries[discovery_table.first_free].initial_source_id = TOS_NODE_ID;
  			discovery_table.entries[discovery_table.first_free].sender_id = TOS_NODE_ID;
  			discovery_table.entries[discovery_table.first_free].forward_cost = 0;
  			discovery_table.entries[discovery_table.first_free].residual_cost = 255;
  			discovery_table.entries[discovery_table.first_free].timestamp = current_time;
  			discovery_table.first_free++;
 
  			send_rreq(destination);
  		}

  	}

  	event void Destination1.fired(){
  		expire_destination(1);
  	}
  	event void Destination2.fired(){
		expire_destination(2);
  	}
  	event void Destination3.fired(){
		expire_destination(3);
  	}
	event void Destination4.fired(){
		expire_destination(4);
  	}
  	event void Destination5.fired(){
		expire_destination(5);
  	}
  	event void Destination6.fired(){
		expire_destination(6);
  	}
  	event void Destination7.fired(){
		expire_destination(7);
  	}
  	event void Destination8.fired(){
		expire_destination(8);
  	}


  	event void AMSend.sendDone(message_t* bufPtr, error_t error) {
    	busy = FALSE;
  	}

  	event message_t* Receive.receive(message_t* mess, void* msg_body, uint8_t len) {

  		am_addr_t source_address;

		source_address = call AMPacket.source(mess);

		switch(len){
			case MSG_REQ_LEN:
				manage_request(source_address, msg_body);
				break;
			case MSG_REPLY_LEN:
				manage_reply(source_address, msg_body);
				break;
			case MSG_DATA_LEN:
				manage_data(source_address, msg_body);
				break;
		}

		return mess;

  	}

  	/*
  	* Functions body
  	*/

  	void manage_request(am_addr_t source_address, void* msg_body){
  		nx_uint32_t current_time;
  		msg_request_t* message;

		uint8_t position = 0;
		uint8_t i = 0;
		bool found = FALSE;
		bool to_update = FALSE;
		
		current_time = call DataTimer.getNow();
		message = (msg_request_t*) msg_body;

		printf("RREQ [%ld] received from %d\n", message->request_id,source_address);

		while(i<discovery_table.first_free && !found){
			if(discovery_table.entries[i].request_id == message->request_id){
				found=TRUE;
				position=i;
			}
			i++;
		}
		if(!found){
			position = discovery_table.first_free;
			discovery_table.first_free++;
			to_update = TRUE;
			printf("Adding RREQ [%ld] to discovery table\n",message->request_id);
		}
		else if(discovery_table.entries[position].forward_cost>(message->forward_hops)+1){
			to_update = TRUE;
			printf("Updating RREQ [%ld] in discovery table\n",message->request_id);
		}else{
			to_update=FALSE;
		}
		if(to_update){
			discovery_table.entries[position].request_id=message->request_id;
			discovery_table.entries[position].initial_source_id=message->initial_source_id;
			discovery_table.entries[position].sender_id=source_address;
			discovery_table.entries[position].forward_cost=(message->forward_hops)+1;
			discovery_table.entries[position].residual_cost=255;
			discovery_table.entries[position].timestamp = current_time;
		}

		if(message -> final_destination_id == TOS_NODE_ID){
			printf("Received RREQ [%ld] from node %d hopping %d\n", message->request_id, source_address, message->forward_hops + 1);
			send_rreply(message,source_address);
		}else if (to_update){
			fwd_request(message);
		}
  	}

	void manage_reply(am_addr_t source_address, void* msg_body){
		nx_uint32_t current_time;
		msg_reply_t* reply_mes;

		uint8_t position = 0;
		uint8_t i = 0;
		uint8_t j = 0;
		bool found = FALSE;
		bool to_update = FALSE;

		current_time = call DataTimer.getNow();
		reply_mes = (msg_reply_t*) msg_body;

		printf("Received RREPLY [%ld] from node %d hopping %d\n",reply_mes->request_id, source_address, reply_mes->backward_hops);

		while(i<routing_table.first_free && !found){
			if(routing_table.entries[i].destination==reply_mes->replyer_node){
				found=TRUE;
				position=i;
			}
			i++;
		}

		i=0;
		current_time = call DataTimer.getNow();
		while(i<discovery_table.first_free && discovery_table.entries[i].request_id!=reply_mes->request_id){
			i++;
		}

		if(i<discovery_table.first_free){
			if(current_time - discovery_table.entries[i].timestamp > DROP_REPLY_MS){
				discovery_table.entries[i].request_id = discovery_table.entries[discovery_table.first_free-1].request_id;
	  			discovery_table.entries[i].initial_source_id = discovery_table.entries[discovery_table.first_free-1].initial_source_id;
	  			discovery_table.entries[i].sender_id = discovery_table.entries[discovery_table.first_free-1].sender_id;
	  			discovery_table.entries[i].forward_cost = discovery_table.entries[discovery_table.first_free-1].forward_cost;
	  			discovery_table.entries[i].residual_cost = discovery_table.entries[discovery_table.first_free-1].residual_cost;
	  			discovery_table.entries[i].timestamp = discovery_table.entries[discovery_table.first_free-1].timestamp;
	  			discovery_table.first_free--;
				printf("Discarding RREPLY for [%ld]\n",reply_mes->request_id);
				return;
			}
		}

		if(!found){
			routing_table.entries[routing_table.first_free].destination = reply_mes -> replyer_node;
			routing_table.entries[routing_table.first_free].next_hop = source_address;
			routing_table.entries[routing_table.first_free].status = TRUE;
			position = routing_table.first_free;
			
			to_update = TRUE;
			start_destination_timer(routing_table.entries[routing_table.first_free].destination);
			routing_table.first_free++;
		}
		
		if(i<discovery_table.first_free){
			if((reply_mes-> backward_hops) <= discovery_table.entries[i].residual_cost){
				discovery_table.entries[i].residual_cost = (reply_mes-> backward_hops)+1;
				routing_table.entries[position].next_hop = source_address;
				routing_table.entries[position].status = TRUE;
				to_update = TRUE;
				start_destination_timer(routing_table.entries[position].destination);
			}
		}


		if(to_update){
			if(reply_mes->requester_node != TOS_NODE_ID){
				fwd_reply(reply_mes, discovery_table.entries[i].sender_id);
			}else{
				printf("Successful RREQ [%ld] conclusion\n",reply_mes->request_id);
				print_routing_table();
				j=0;
				found=FALSE;
				while(j<data_table.first_free && !found){
					if(data_table.entries[j].request_id==reply_mes->request_id){
						found=TRUE;
					}
					j++;
				}
				j--;
				if(found){
					fwd_data(routing_table.entries[position].next_hop,&(data_table.entries[j].message));
				}
			}
		}
	}

	void manage_data(am_addr_t source_address, void* msg_body){

		msg_data_t* data_mes;

		uint8_t i = 0;
		uint8_t k = 0;
		bool found = FALSE;

		data_mes = (msg_data_t*) msg_body;
	
		if(data_mes->final_destination_id == TOS_NODE_ID){
			printf("Successful DATA message reception from %d with content %ld \n", data_mes->source_id, data_mes->data);
		}
		else{
			i = 0;
			while(i<routing_table.first_free){
	  			if(routing_table.entries[i].destination==data_mes->final_destination_id && routing_table.entries[i].status){
	  				found=TRUE;
	  				k=i;
	  				i=routing_table.first_free;
	  			}
	  			i++;
  			}	
	  		if(found){
	  			fwd_data(routing_table.entries[k].next_hop,data_mes);
	  		}
	  		else{
	  			data_table.entries[data_table.first_free].request_id=local_msg_id;
	  			data_table.entries[data_table.first_free].message.data=data_mes->data;
	  			data_table.entries[data_table.first_free].message.final_destination_id=data_mes->final_destination_id;
	  			data_table.entries[data_table.first_free].message.source_id=data_mes->source_id;
	  			data_table.first_free++;

	  			discovery_table.entries[discovery_table.first_free].request_id=local_msg_id;
	  			discovery_table.entries[discovery_table.first_free].initial_source_id=TOS_NODE_ID;
	  			discovery_table.entries[discovery_table.first_free].sender_id=TOS_NODE_ID;
	  			discovery_table.entries[discovery_table.first_free].forward_cost=0;
	  			discovery_table.entries[discovery_table.first_free].residual_cost=255;
	  			discovery_table.first_free++;
	  			send_rreq(data_mes->final_destination_id);
	  		}
		}
		
	}

  	void send_rreq(nx_uint16_t dest){

		msg_request_t* message;

		if(!busy){
			message = (msg_request_t*)(call Packet.getPayload(&packet,sizeof(msg_request_t)));
			
			if(message == NULL){
				return;
			}

			message->final_destination_id = dest;  
			message->initial_source_id = TOS_NODE_ID;
			message->request_id = local_msg_id++;
			message->forward_hops=0;

			if (call AMSend.send(AM_BROADCAST_ADDR, &packet, sizeof(msg_request_t)) == SUCCESS) {
				busy = TRUE;
				printf("Sent RREQ [%ld] to node %d\n",message->request_id, dest);
	      	}
		}

	}


	void send_data(nx_uint16_t next_hop, nx_uint32_t random_data){
		msg_data_t* message;

		if(!busy){
			message = (msg_data_t*)(call Packet.getPayload(&packet,sizeof(msg_data_t)));
			
			if(message == NULL){
				return;
			}

			message->source_id=TOS_NODE_ID;
			message->final_destination_id = destination;  
			message->data=random_data;

			if (call AMSend.send(next_hop, &packet, sizeof(msg_data_t)) == SUCCESS) {
				busy = TRUE;
				printf("Sent DATA message to node %d\n",destination);
			}
		}

	}

	void fwd_data(nx_uint16_t next_hop, msg_data_t* msg){
		msg_data_t* message;

		if(!busy){
			message = (msg_data_t*)(call Packet.getPayload(&packet,sizeof(msg_data_t)));
			
			if(message == NULL){
				return;
			}

			message->final_destination_id = msg->final_destination_id;  
			message->source_id=msg->source_id;
			message->data=msg->data;

			if (call AMSend.send(next_hop, &packet, sizeof(msg_data_t)) == SUCCESS) {
				busy = TRUE;
				printf("Forwarded DATA message, originated by %d, to %d\n", msg->source_id, next_hop);
			}
		}
	}



  	void fwd_reply(msg_reply_t* route_reply, nx_uint16_t sender_id){
  		msg_reply_t* reply_mes;

  		if(!busy){
	  		reply_mes = (msg_reply_t*)(call Packet.getPayload(&packet,sizeof(msg_reply_t)));
			
			if(reply_mes == NULL){
				return;
			}

			reply_mes->requester_node=route_reply->requester_node;
			reply_mes->replyer_node=route_reply->replyer_node;
			reply_mes->request_id=route_reply->request_id;
			reply_mes->backward_hops=(route_reply->backward_hops + 1);

			if (call AMSend.send(sender_id, &packet, sizeof(msg_reply_t)) == SUCCESS) {
				busy = TRUE;
				printf("Forwarded RREPLY of [%ld] originated by %d to %d\n", reply_mes->request_id, reply_mes->replyer_node, sender_id);
	      	}
		}
  	}


  	void send_rreply(msg_request_t* req_msg, am_addr_t source_address){
  		msg_reply_t* reply_mes;

  		if(!busy){
	  		reply_mes = (msg_reply_t*)(call Packet.getPayload(&packet,sizeof(msg_reply_t)));
			
			if(reply_mes == NULL){
				return;
			}

			reply_mes->requester_node = req_msg->initial_source_id;
			reply_mes->replyer_node = req_msg->final_destination_id;
			reply_mes->request_id = req_msg->request_id;
			reply_mes->backward_hops = 0;

			if (call AMSend.send(source_address, &packet, sizeof(msg_reply_t)) == SUCCESS) {
				printf("Sending RREPLY of [%ld] to %d\n",reply_mes->request_id,source_address );
				busy = TRUE;
	      	}
		}

  	}

  	void fwd_request(msg_request_t* request_msg){
  		msg_request_t* message;
  		
  		if(!busy){
			message = (msg_request_t*)(call Packet.getPayload(&packet,sizeof(msg_request_t)));
			
			if(message == NULL){
				return;
			}

			message->final_destination_id = request_msg->final_destination_id;  
			message->initial_source_id = request_msg->initial_source_id;
			message->request_id = request_msg->request_id;
			message->forward_hops=(request_msg->forward_hops)+1;

			if (call AMSend.send(AM_BROADCAST_ADDR, &packet, sizeof(msg_request_t)) == SUCCESS) {
				busy = TRUE;
				printf("Forwarded RREQ [%ld]\n", message->request_id);
	      	}
		}
  	}

	void start_destination_timer(uint16_t routing_destination){
			print_routing_table();
		switch (routing_destination){
			case 1:
				call Destination1.startOneShot(EXPIRE_ROUTING_MS);
				break;
			case 2:
				call Destination2.startOneShot(EXPIRE_ROUTING_MS);
				break;
			case 3:
				call Destination3.startOneShot(EXPIRE_ROUTING_MS);
				break;
			case 4:
				call Destination4.startOneShot(EXPIRE_ROUTING_MS);
				break;
			case 5:
				call Destination5.startOneShot(EXPIRE_ROUTING_MS);
				break;
			case 6:
				call Destination6.startOneShot(EXPIRE_ROUTING_MS);
				break;
			case 7:
				call Destination7.startOneShot(EXPIRE_ROUTING_MS);
				break;
			case 8:
				call Destination8.startOneShot(EXPIRE_ROUTING_MS);
				break;
		}
	}

	void expire_destination (uint16_t routing_destination){
  		uint16_t i=0;
  		while(i<routing_table.first_free && routing_table.entries[i].destination != routing_destination){
  			i++;
  		}
  		if(i<routing_table.first_free){
			routing_table.entries[i].status = FALSE;
  		}
  		printf("Expired destination %d entry\n", routing_destination);
  		print_routing_table();
  	}

	void print_routing_table(){
		uint16_t i = 0;
		i = 0;
		printf("|| DESTINATION | NEXT HOP | STATUS ||\n");
		while(i < routing_table.first_free){
			printf("||      %d      |     %d    |   %d    ||\n",routing_table.entries[i].destination, routing_table.entries[i].next_hop, routing_table.entries[i].status);
			i++;
		}
	}

	void print_routing_discovery_table(){
		uint16_t i = 0;
		printf("||RREQID|SOURCE|SENDER|FWD_COST|RES_COST||\n");
		while(i<discovery_table.first_free){
			printf("|| %ld |  %d  |  %d  |  %d  |  %d  ||\n", discovery_table.entries[i].request_id,discovery_table.entries[i].initial_source_id, discovery_table.entries[i].sender_id, discovery_table.entries[i].forward_cost, discovery_table.entries[i].residual_cost);	
			i++;
		}
	}

}
