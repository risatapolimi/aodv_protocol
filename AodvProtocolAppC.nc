#include "aodv_protocol.h"

configuration AodvProtocolAppC{}

implementation {
	
	components AodvProtocolC, MainC, new TimerMilliC() as Timer;
	components ActiveMessageC as Radio;
	components new AMSenderC(AM_CHANNEL);
	components new AMReceiverC(AM_CHANNEL);
	components RandomC;
	components SerialPrintfC;

	components new TimerMilliC() as DestinationTimer1;
	components new TimerMilliC() as DestinationTimer2;
	components new TimerMilliC() as DestinationTimer3;
	components new TimerMilliC() as DestinationTimer4;
	components new TimerMilliC() as DestinationTimer5;
	components new TimerMilliC() as DestinationTimer6;
	components new TimerMilliC() as DestinationTimer7;
	components new TimerMilliC() as DestinationTimer8;	
	
	AodvProtocolC.Boot -> MainC.Boot;

	AodvProtocolC.AMControl -> Radio;
	AodvProtocolC.AMPacket -> AMSenderC;
	AodvProtocolC.Receive -> AMReceiverC;
	AodvProtocolC.AMSend -> AMSenderC;
	AodvProtocolC.Packet -> AMSenderC;

	AodvProtocolC.DataTimer -> Timer;

	AodvProtocolC.Destination1 -> DestinationTimer1;
	AodvProtocolC.Destination2 -> DestinationTimer2;
	AodvProtocolC.Destination3 -> DestinationTimer3;
	AodvProtocolC.Destination4 -> DestinationTimer4;
	AodvProtocolC.Destination5 -> DestinationTimer5;
	AodvProtocolC.Destination6 -> DestinationTimer6;
	AodvProtocolC.Destination7 -> DestinationTimer7;
	AodvProtocolC.Destination8 -> DestinationTimer8;

	AodvProtocolC.Random -> RandomC;
}
